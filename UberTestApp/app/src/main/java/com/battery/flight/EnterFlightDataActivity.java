package com.battery.flight;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.battery.ubertestapp.R;

import java.util.concurrent.ThreadPoolExecutor;

/**
 * Created by namrata on 2/26/15.
 */
public class EnterFlightDataActivity extends Activity {

    EditText enterCarrier;
    EditText enterFlightNumber;
    DatePicker selectDate;
    Button flightInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_flight_data);
        enterCarrier = (EditText) findViewById(R.id.enter_carrier);
        enterFlightNumber = (EditText) findViewById(R.id.enter_flightnumber);
        selectDate = (DatePicker) findViewById(R.id.select_date);
        flightInfo = (Button) findViewById(R.id.flight_info);

        flightInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (enterCarrier!=null && enterFlightNumber !=null)
                {
                    Intent intent = new Intent(getApplicationContext(), FlightStatusActivity.class);
                    intent.putExtra("carrierName" , enterCarrier.getText().toString());
                    intent.putExtra("flightNumber" , enterFlightNumber.getText().toString());
                    intent.putExtra("day" , selectDate.getDayOfMonth());
                    intent.putExtra("month" , selectDate.getMonth());
                    intent.putExtra("year" , selectDate.getYear());
                    startActivity(intent);
                }
                else {
                    Toast.makeText(getApplicationContext(), "Please enter required information", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
