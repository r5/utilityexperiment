package com.battery.flight;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.TextView;

import com.battery.ubertestapp.R;

import java.util.List;

/**
 * Created by namrata on 2/25/15.
 */
public class FlightStatusActivity extends Activity {

    public static String carrierName, flightNumber;
    public static int day, month, year;
    FlightAPI flightAPI;
    TextView flight;
    TextView flightInfo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flight_status);
        Intent intent = getIntent();
        carrierName = intent.getStringExtra("carrierName");
        flightNumber = intent.getStringExtra("flightNumber");
        day = intent.getIntExtra("day", 0);
        month = intent.getIntExtra("month", 0);
        year = intent.getIntExtra("year" , 0);
        flight = (TextView) findViewById(R.id.flight_number);
        flightInfo = (TextView) findViewById(R.id.flight_status);
        new FlightTask().execute();
    }

    private class FlightTask extends AsyncTask<Void, Void, List<FlightData>>{
        @Override
        protected List<FlightData> doInBackground(Void... params) {
            List<FlightData> data = null;
            try {
                flightAPI = new FlightAPI();
                data = flightAPI.flightDataList();
            }catch (Exception e){
                e.printStackTrace();
            }
            return data;
        }

        @Override
        protected void onPostExecute(List<FlightData> flightDatas) {
            super.onPostExecute(flightDatas);
            if (flightDatas.size() >0){
                FlightData flightData = flightDatas.get(0);
                flight.setText(carrierName + " - "+ flightNumber );
                flightInfo.setText("Departure Airport Code : " + flightData.getDepartureAirportCode()
                                +"\n" + "Departure Time : " + flightData.getDepartureTime()
                                + "\n" + "Arrival Airport Code : " + flightData.getArrivalAirportCode()
                                +"\n" + "Arrival Time : " + flightData.getArrivalTime()
                                +"\n" + "Arrival Terminal : " + flightData.getArrivalTerminal());
            }
        }
    }

}
