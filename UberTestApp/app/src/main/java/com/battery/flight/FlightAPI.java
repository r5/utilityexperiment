package com.battery.flight;

import android.net.Uri;

import com.battery.main.GetRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by namrata on 2/25/15.
 */
public class FlightAPI {

    GetRequest getRequest = new GetRequest();
    public String flightNumber = FlightStatusActivity.flightNumber;
    public String carrierName = FlightStatusActivity.carrierName;
    public int day = FlightStatusActivity.day;
    public int month = FlightStatusActivity.month;
    public int year = FlightStatusActivity.year;

    public List<FlightData> flightDataList() throws Exception {
        List<FlightData> flightDatas = new ArrayList<FlightData>();

        Uri.Builder builder = Uri.parse("https://api.flightstats.com/flex/schedules/rest/v1/json/flight/" + carrierName + "/" + flightNumber + "/departing/" + year + "/" + (month+1) + "/" + day).buildUpon();
        builder.appendQueryParameter("appId", "***")
                .appendQueryParameter("appKey", "***");

        String url = builder.build().toString();
        String outputData = getRequest.GET(url);
        System.out.println(url);
        System.out.println(outputData);
        JSONObject jsonObject;

        try {
            jsonObject = new JSONObject(outputData);
            FlightData flightData = new FlightData();
            JSONArray scheduledFlights = jsonObject.getJSONArray("scheduledFlights");
            for (int i = 0; i < scheduledFlights.length(); i++) {
                JSONObject flightInfo = scheduledFlights.getJSONObject(i);
                flightData.setArrivalAirportCode(flightInfo.getString("arrivalAirportFsCode"));
                flightData.setDepartureAirportCode(flightInfo.getString("departureAirportFsCode"));
                flightData.setArrivalTerminal(flightInfo.getString("arrivalTerminal"));
                flightData.setDepartureTime(flightInfo.getString("departureTime"));
                flightData.setArrivalTime(flightInfo.getString("arrivalTime"));
                flightDatas.add(flightData);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flightDatas;
    }
}