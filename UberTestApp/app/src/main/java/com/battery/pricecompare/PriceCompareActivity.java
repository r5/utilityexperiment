package com.battery.pricecompare;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.battery.ubertestapp.R;

import java.util.List;

/**
 * Created by namrata on 2/24/15.
 */
public class PriceCompareActivity extends Activity {

    public static String query;
    ListView view;
    PriceApi priceApi;
    ItemAdapter itemAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_price_compare);
        Intent intent = getIntent();
        query = intent.getStringExtra("item");
        view = (ListView) findViewById(R.id.item_list);
        new APITask().execute();
    }

    private class APITask extends AsyncTask<Void, Void, List<ItemData>> {

        @Override
        protected List doInBackground(Void... params) {
            List<ItemData> data = null;
            try {
                priceApi = new PriceApi();
                data = priceApi.itemDataList();

            }catch (Exception e){
                e.printStackTrace();
            }
            return data;
        }

        @Override
        protected void onPostExecute(List<ItemData> result) {
            super.onPostExecute(result);
            if (result != null) {
                itemAdapter = new ItemAdapter(PriceCompareActivity.this, 0, result);
                view.setAdapter(itemAdapter);
            } else {
                Toast.makeText(PriceCompareActivity.this, "No data", Toast.LENGTH_LONG).show();
            }
            return;
        }
    }

}
