package com.battery.pricecompare;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.battery.pricecompare.PriceCompareActivity;
import com.battery.ubertestapp.R;

/**
 * Created by namrata on 2/24/15.
 */
public class EnterItemActivity extends Activity {

    EditText enterItem;
    Button comparePrice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_item);
        enterItem = (EditText) findViewById(R.id.enter_item);
        comparePrice = (Button) findViewById(R.id.item_info);

        comparePrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (enterItem!=null) {
                    Intent intent = new Intent(getApplicationContext(), PriceCompareActivity.class);
                    intent.putExtra("item", enterItem.getText().toString());
                    startActivity(intent);
                }else  {
                    Toast.makeText(getApplicationContext(), "Please enter an item to comapre Prices", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
