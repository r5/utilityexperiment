package com.battery.pricecompare;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.battery.ubertestapp.R;

import java.util.List;

/**
 * Created by namrata on 2/24/15.
 */
public class ItemAdapter extends ArrayAdapter {
    private Context mContext;
    private List<ItemData> items;

    public ItemAdapter(Context context, int resource, List<ItemData> items) {
        super(context, resource);
        mContext = context;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    private static class ViewHolder {
        private TextView company;
        private TextView itemName;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        ViewHolder viewHolder;
        if (rowView==null){
            rowView = ((Activity) mContext).getLayoutInflater().inflate(R.layout.price_list_item, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.company = (TextView) rowView.findViewById(R.id.item_company);
            viewHolder.itemName = (TextView) rowView.findViewById(R.id.item_name);
            rowView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) rowView.getTag();
        }
        try {
            ItemData itemData = items.get(position);
            viewHolder.company.setText(itemData.getSiteName());
            viewHolder.itemName.setText(itemData.getProdName()
                    + "\n" + "User Rating" + itemData.getRating()
                    + "\n" + "Price : Rs. " + itemData.getPrice());
        }catch (Exception e){
            e.printStackTrace();
        }
        return rowView;
    }
}
