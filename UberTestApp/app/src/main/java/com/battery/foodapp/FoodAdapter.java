package com.battery.foodapp;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.battery.ubertestapp.R;

import java.util.List;

/**
 * Created by namrata on 2/10/15.
 */
public class FoodAdapter extends ArrayAdapter {

    private Context mContext;
    private List<FoodData> foods;

    public FoodAdapter(Context context, int resource, List<FoodData> foods) {
        super(context, resource);
        mContext = context;
        this.foods = foods;
    }

    @Override
    public int getCount() {
        return foods.size();
    }

    @Override
    public Object getItem(int position) {
        return foods.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        ViewHolder viewHolder;
        if (rowView==null){
            rowView = ((Activity) mContext).getLayoutInflater().inflate(R.layout.food_card, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.name = (TextView) rowView.findViewById(R.id.name_info);
            viewHolder.cuisines = (TextView) rowView.findViewById(R.id.cuisine_info);
            viewHolder.address = (TextView) rowView.findViewById(R.id.address_info);
            viewHolder.phone = (TextView) rowView.findViewById(R.id.phone_info);
            viewHolder.timings = (TextView) rowView.findViewById(R.id.timing_info);
            viewHolder.isOpenNow = (TextView) rowView.findViewById(R.id.open_info);
            viewHolder.userRating = (TextView) rowView.findViewById(R.id.rating);
            rowView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) rowView.getTag();
        }

        FoodData foodData = foods.get(position);
        double rating = foodData.getUserRating();
        String rate = String.valueOf(rating*2);
        viewHolder.name.setText(foodData.getName().trim());
        viewHolder.cuisines.setText("Cuisines" + " : " + foodData.getCuisines());
        viewHolder.address.setText("Address" + " : " + foodData.getAddress().trim());
        viewHolder.phone.setText("Phone" + " : " + foodData.getPhone().trim());
        viewHolder.timings.setText("Timings" + " : " + foodData.getTimings().trim());
        viewHolder.isOpenNow.setText(foodData.getIsOpenNow().trim());
        viewHolder.userRating.setText("User Rating" + " : " + rate);
        return rowView;
    }

    private static class ViewHolder {
        private TextView name;
        private TextView cuisines;
        private TextView address;
        private TextView phone;
        private TextView timings;
        private TextView isOpenNow;
        private TextView userRating;
    }
}

