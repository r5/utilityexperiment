package com.battery.foodapp;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import com.battery.main.GpsService;
import com.battery.ubertestapp.R;

import java.util.List;

/**
 * Created by namrata on 2/9/15.
 */

public class RestaurantsActivity extends Activity {

    public static String name;
    public String locality;
    public static double lat, lon;
    ListView view;
    FoodAdapter foodAdapter;
    GpsService gpsService;
    FoodAPI foodAPI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food);
        Intent intent = getIntent();
        name = intent.getStringExtra("Name");
        locality = intent.getStringExtra("Locality");
        lat = intent.getDoubleExtra("Lat", 28.00);
        lon = intent.getDoubleExtra("Lon", 77.00);
        view = (ListView) findViewById(R.id.food_list);
        new APITask().execute();
    }

    private class APITask extends AsyncTask<Void, Void, List<FoodData>> {

        @Override
        protected List doInBackground(Void... params) {
            List<FoodData> data = null;
            gpsService = new GpsService(RestaurantsActivity.this);
            try {
                if (gpsService.canGetLocation()){
                    foodAPI = new FoodAPI(lat,lon);
                    data = foodAPI.foodDataList();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
            return data;
        }

        @Override
        protected void onPostExecute(List<FoodData> result) {
            super.onPostExecute(result);
            if (result != null) {
                foodAdapter = new FoodAdapter(RestaurantsActivity.this, 0, result);
                view.setAdapter(foodAdapter);
            } else {
                Toast.makeText(RestaurantsActivity.this, "No data", Toast.LENGTH_LONG).show();
            }
            return;
        }
    }

}

