package com.battery.foodapp;

import android.net.Uri;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by namrata on 2/10/15.
 */
public class FoodAPI {

    double lat = RestaurantsActivity.lat;
    double lon = RestaurantsActivity.lon;
    String name = RestaurantsActivity.name;

    public FoodAPI(double lat, double lon) {
        this.lat = lat;
        this.lon = lon;
    }

    public List<FoodData> foodDataList() throws Exception {
        List<FoodData> foodDatas = new ArrayList<FoodData>();
        Uri.Builder builder = Uri.parse("https://api.zomato.com/v2/search.json/near").buildUpon();
        builder.appendQueryParameter("lat", String.valueOf(lat))
                .appendQueryParameter("lon", String.valueOf(lon))
                .appendQueryParameter("count" , "100")
                .appendQueryParameter("q" , name);

        String url = builder.build().toString();
        String outputData =GET(url);
        System.out.println(outputData);
        System.out.println(url);
        JSONObject jsonObject;

        try {
            jsonObject = new JSONObject(outputData);
            JSONArray restaurants = jsonObject.getJSONArray("restaurants");
            for (int i = 0; i < restaurants.length(); i++) {
                JSONObject restaurant = restaurants.getJSONObject(i);
                JSONObject info = restaurant.getJSONObject("restaurant");
                FoodData foodData = new FoodData();
                foodData.setName(info.getString("name"));
                JSONObject location_info = info.getJSONObject("location");
                foodData.setAddress(location_info.getString("address"));
                foodData.setCuisines(info.getString("cuisines"));
                foodData.setPhone(info.getString("phone"));
                foodData.setTimings(info.getString("timings"));
                int openStatus = info.getInt("is_open_now");
                if (openStatus == 1){
                    foodData.setIsOpenNow("Open");
                }else {
                    foodData.setIsOpenNow("Closed");
                }
                JSONObject rating = info.getJSONObject("user_rating");
                foodData.setUserRating(rating.getDouble("aggregate_rating"));
                foodDatas.add(foodData);
            }
        }catch (JSONException e){
            e.printStackTrace();
            Log.e("FoodAPI", e.getMessage(), e);
        }
        return foodDatas;
    }

    public String GET(String url) throws Exception{
        //create HTTPClient
        HttpClient httpclient = new DefaultHttpClient();
        HttpGet request = new HttpGet(url);
        request.setHeader("X-Zomato-API-Key" ,"***");

        HttpResponse httpResponse = httpclient.execute(request);

        System.out.println("Response Code : "
                + httpResponse.getStatusLine().getStatusCode());

        BufferedReader rd = new BufferedReader(
                new InputStreamReader(httpResponse.getEntity().getContent()));

        StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = rd.readLine())!=null){
            result.append(line);
        }
        return result.toString();
    }
}
