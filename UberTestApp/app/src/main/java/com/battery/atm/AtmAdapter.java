package com.battery.atm;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.battery.ubertestapp.R;

import java.util.List;

/**
 * Created by namrata on 2/12/15.
 */
public class AtmAdapter extends ArrayAdapter {

    private Context mContext;
    private List<AtmData> atms;

    public AtmAdapter(Context context, int resource, List<AtmData> atms) {
        super(context, resource);
        mContext = context;
        this.atms = atms;
    }

    @Override
    public int getCount() {
        return atms.size();
    }

    @Override
    public Object getItem(int position) {
        return atms.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        ViewHolder viewHolder;
        if (rowView==null){
            rowView = ((Activity) mContext).getLayoutInflater().inflate(R.layout.atm_list_item, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.name = (TextView) rowView.findViewById(R.id.atm_name);
            viewHolder.address = (TextView) rowView.findViewById(R.id.atm_address);
            rowView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) rowView.getTag();
        }
        AtmData atmData = atms.get(position);
        viewHolder.name.setText(atmData.getName().trim());
        viewHolder.address.setText(atmData.getAddress().trim());
        return rowView;
    }

    private static class ViewHolder {
        private TextView name;
        private TextView address;
    }
}
