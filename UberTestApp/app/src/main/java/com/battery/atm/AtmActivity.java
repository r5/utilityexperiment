package com.battery.atm;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.battery.main.GpsService;
import com.battery.ubertestapp.R;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

public class AtmActivity extends FragmentActivity {

    private GoogleMap mMap;
    public static double lat, lon;
    public static String name;
    GpsService gpsService;
    AtmAdapter atmAdapter;
    ListView atmList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_atm);
        Intent intent = getIntent();
        lat = intent.getDoubleExtra("Lat", 28.00);
        lon = intent.getDoubleExtra("Lon", 77.00);
        name = intent.getStringExtra("Name");
        gpsService = new GpsService(AtmActivity.this);
        setUpMapIfNeeded();
        new ShowAtm().execute();
        atmList = (ListView) findViewById(R.id.atm_list);
    }
    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }
    private void setUpMapIfNeeded() {
        if (mMap == null) {
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            if (mMap != null) {
                setUpMap();
            }
        }
    }
    private void setUpMap() {
        mMap.addMarker(new MarkerOptions().position(new LatLng(lat, lon)).title("You are here").icon(BitmapDescriptorFactory.fromResource(R.drawable.blue))).showInfoWindow();
        CameraUpdate center= CameraUpdateFactory.newLatLng(new LatLng(lat,lon));
        CameraUpdate zoom=CameraUpdateFactory.zoomTo(14);
        mMap.moveCamera(center);
        mMap.animateCamera(zoom);
    }


    class ShowAtm extends AsyncTask<Void, Void, List<AtmData>>{

        @Override
        protected List<AtmData> doInBackground(Void... params) {
            List<AtmData> data = null;
            try {
                if (gpsService.canGetLocation()){
                    AtmLocator atmLocator = new AtmLocator(lat,lon);
                    data = atmLocator.locationDataList();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
            return data;
        }

        @Override
        protected void onPostExecute(List<AtmData> atmDatas) {
            super.onPostExecute(atmDatas);
            if (atmDatas != null) {

                for (final AtmData atmData: atmDatas){
                    mMap.addMarker(new MarkerOptions().position(new LatLng(atmData.getLat(), atmData.getLon())).title(atmData.getName())).showInfoWindow();
                }

                atmAdapter = new AtmAdapter(AtmActivity.this, 0, atmDatas);
                atmList.setAdapter(atmAdapter);

                atmList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        AtmData atmData = (AtmData) atmAdapter.getItem(position);
                        CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(atmData.getLat(), atmData.getLon()));
                        CameraUpdate zoom = CameraUpdateFactory.zoomTo(15);
                        mMap.moveCamera(center);
                        mMap.animateCamera(zoom);
                        // get marker instace and show popup
                    }
                });
            }else {
                mMap.addMarker(new MarkerOptions().position(new LatLng(lat, lon)).title("ATM"));
            }
        }
    }
}

