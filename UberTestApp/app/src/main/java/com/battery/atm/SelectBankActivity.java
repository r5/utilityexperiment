package com.battery.atm;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.view.View.OnClickListener;

import com.battery.main.GpsService;
import com.battery.ubertestapp.R;

/**
 * Created by namrata on 2/12/15.
 */
public class SelectBankActivity extends Activity{

    Button atm1;
    Button atm2;
    Button atm3;
    Button atmAll;

    GpsService gpsService;
    public double lat , lon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_atm_name);
        gpsService = new GpsService(SelectBankActivity.this);
        atm1 = (Button) findViewById(R.id.atm_1);
        atm2 = (Button) findViewById(R.id.atm_2);
        atm3 = (Button) findViewById(R.id.atm_3);
        atmAll = (Button) findViewById(R.id.atm_all);

        if (gpsService.canGetLocation()){
            lat = gpsService.getLatitude();
            lon = gpsService.getLongitude();
            atm1.setOnClickListener(new OnClickListener() {
                public void onClick(View arg0) {
                    Intent intent = new Intent(getApplicationContext(), AtmActivity.class);
                    intent.putExtra("Lat" , lat);
                    intent.putExtra("Lon" , lon);
                    intent.putExtra("Name" , "HDFC");
                    startActivity(intent);
                }
            });

            atm2.setOnClickListener(new OnClickListener() {
                public void onClick(View arg0) {
                    Intent intent = new Intent(getApplicationContext(), AtmActivity.class);
                    intent.putExtra("Lat" , lat);
                    intent.putExtra("Lon" , lon);
                    intent.putExtra("Name" , "SBI");
                    startActivity(intent);
                }
            });

            atm3.setOnClickListener(new OnClickListener() {
                public void onClick(View arg0) {
                    Intent intent = new Intent(getApplicationContext(), AtmActivity.class);
                    intent.putExtra("Lat" , lat);
                    intent.putExtra("Lon" , lon);
                    intent.putExtra("Name" , "Citibank");
                    startActivity(intent);
                }
            });

            atmAll.setOnClickListener(new OnClickListener() {
                public void onClick(View arg0) {
                    Intent intent = new Intent(getApplicationContext(), AtmActivity.class);
                    intent.putExtra("Lat" , lat);
                    intent.putExtra("Lon" , lon);
                    intent.putExtra("Name" , "");
                    startActivity(intent);
                }
            });

        }

    }
}
