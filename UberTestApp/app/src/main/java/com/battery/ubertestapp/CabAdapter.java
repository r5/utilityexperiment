package com.battery.ubertestapp;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by namrata on 1/28/15.
 */
public class CabAdapter extends ArrayAdapter {

    private Context mContext;
    private List<CabData> cabs;

    public CabAdapter(Context context, int resource, List<CabData> cabs) {
        super(context, resource);
        mContext = context;


        this.cabs = cabs;
    }

    public void addCabs(List<CabData> cabDatas) {
        this.cabs.addAll(cabDatas);
    }

    @Override
    public void clear() {
        super.clear();
        this.cabs = new ArrayList<>();
    }

    public void setCabs(List<CabData> companies) {
        this.cabs = cabs;
    }

    @Override
    public int getCount() {
        return cabs.size();
    }

    @Override
    public Object getItem(int position) {
        return cabs.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = convertView;
        ViewHolder viewHolder;
        if (rowView==null){
            rowView = ((Activity) mContext).getLayoutInflater().inflate(R.layout.cab_list_item, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.cabCompany = (TextView) rowView.findViewById(R.id.cabCompany);
            viewHolder.localizedDisplayName = (TextView) rowView.findViewById(R.id.product_name);
            viewHolder.estimate = (TextView) rowView.findViewById(R.id.time);
            rowView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) rowView.getTag();
        }
        CabData cabdata = cabs.get(position);
        long time = Long.valueOf(cabdata.getEstimate().trim());
        long time_min;
        String cabCompany = cabdata.cabCompany;
        if (cabCompany.equals(CabData.CAB_OLA)){
            time_min = time;
        }
        else if (cabCompany.equals(CabData.CAB_TFS)){
            time_min = time;
        }
        else if (cabCompany.equals(CabData.CAB_MERU)){
            time_min = time;
        }
        else {
            time_min = time/60;
        }
        viewHolder.localizedDisplayName.setText(cabdata.getLocalizedDisplayNameDisplayName().trim());
        viewHolder.estimate.setText(time_min+ " min");
        viewHolder.cabCompany.setText(cabdata.cabCompany.trim());
        return rowView;
    }

    private static class ViewHolder {
        private TextView localizedDisplayName;
        private TextView estimate;
        private TextView cabCompany;
    }
}
