package com.battery.ubertestapp;

/**
 * Created by namrata on 1/28/15.
 */

import android.location.Location;
import android.net.Uri;

import com.battery.main.GetRequest;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

public class APICall {

    static double x = CabListActivity.x;
    static double y = CabListActivity.y;
    GetRequest getRequest = new GetRequest();

    public APICall(Location location) {
        this.x = location.getLatitude();
        this.y = location.getLongitude();
    }

    public APICall(double lat, double lon) {
        this.x = lat;
        this.y = lon;

    }
    //UBER Cabs

    public List<CabData> ubercabList() throws Exception{

        List<CabData> cabDatas = new ArrayList<CabData>();
        Uri.Builder builder = Uri.parse("https://api.uber.com/v1/estimates/time").buildUpon();
        builder.appendQueryParameter("server_token", "***")
                .appendQueryParameter("start_latitude", String.valueOf(x))
                .appendQueryParameter("start_longitude", String.valueOf(y));

        String url = builder.build().toString();

        System.out.println(url);
        String outputData =getRequest.GET(url);
        System.out.println(outputData);
        JSONObject json;

        try {
            json = new JSONObject(outputData);
            JSONArray jsonArray = json.getJSONArray("times");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                CabData cabData = new CabData();
                cabData.cabCompany = CabData.CAB_UBER;
                cabData.displayName = jsonObject.getString("display_name");
                cabData.productId = jsonObject.getString("product_id");
                cabData.estimate = jsonObject.getString("estimate");
                cabData.localizedDisplayName = jsonObject.getString("localized_display_name");
                cabDatas.add(cabData);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return cabDatas;
    }

    //OLA Cabs

    public List<CabData> olacabList() throws Exception {
        List<CabData> cabDatas = new ArrayList<CabData>();
        String result = URLDecoder.decode("***", "UTF-8");
        Uri.Builder builder = Uri.parse("http://mapi.olacabs.com/v3/cab/info").buildUpon();
        builder.appendQueryParameter("enable_new_state","true")
                .appendQueryParameter("custom_lat", String.valueOf(x))
                .appendQueryParameter("custom_lng", String.valueOf(y))
                .appendQueryParameter("user_id", result);

        String url = builder.build().toString();
        String outputData =GETOla(url);
        System.out.println(outputData);
        System.out.println(url);
        JSONObject json;

        try {
            json = new JSONObject(outputData);
            JSONArray jsonArray = json.getJSONArray("cab_categories");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                CabData cabData = new CabData();
                Boolean cab_availability = jsonObject.getBoolean("cab_availability");
                if (cab_availability) {
                    cabData.cabCompany = CabData.CAB_OLA;
                    cabData.localizedDisplayName = jsonObject.getString("display_name");
                    cabData.productId = jsonObject.getString("id");
                    JSONObject durationObject = jsonObject.getJSONObject("duration");
                    cabData.estimate = durationObject.getString("value");
                    cabDatas.add(cabData);
                }
            }
        }catch (JSONException e){
            e.printStackTrace();
            }
        return cabDatas;
    }

    public String GETOla(String url) throws Exception{
        //create HTTPClient
        HttpClient httpclient = new DefaultHttpClient();
        HttpGet request = new HttpGet(url);
        request.setHeader("client","***");
        request.setHeader("api-key","***");
        request.setHeader("enable_auto","true");

        HttpResponse httpResponse = httpclient.execute(request);

        System.out.println("Response Code : "
                + httpResponse.getStatusLine().getStatusCode());

        BufferedReader rd = new BufferedReader(
                new InputStreamReader(httpResponse.getEntity().getContent()));

        StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = rd.readLine())!=null){
            result.append(line);
        }
    return result.toString();
    }

    //TFS Cabs

    public List<CabData> tfscabList() throws Exception {
        List<CabData> cabDatas = new ArrayList<CabData>();
        Uri.Builder builder = Uri.parse("http://iospush.taxiforsure.com/getNearestDriversForApp/").buildUpon();
        builder.appendQueryParameter("longitude", String.valueOf(y))
                .appendQueryParameter("latitude", String.valueOf(x))
                .appendQueryParameter("density", "***");

        String url = builder.build().toString();
        String outputData =getRequest.GET(url);
        System.out.println(outputData);
        System.out.println(url);
        JSONObject json;

        try {
            json = new JSONObject(outputData);
            JSONObject responseData = json.getJSONObject("response_data");
            JSONArray data = responseData.getJSONArray("data");
            for (int i = 0; i < data.length(); i++) {
                JSONObject jsonObject = data.getJSONObject(i);
                CabData cabData = new CabData();
                cabData.cabCompany = CabData.CAB_TFS;
                cabData.estimate = jsonObject.getString("duration");
                cabData.localizedDisplayName = jsonObject.getString("carType");
                cabDatas.add(cabData);
            }

        }catch (JSONException e){
            e.printStackTrace();
        }
        return cabDatas;
    }

    //Meru Cabs

    public List<CabData> merucabList() throws Exception {
        List<CabData> cabDatas = new ArrayList<CabData>();
        Uri.Builder builder = Uri.parse("http://mobileapp.merucabs.com/NearByCab_Eve/GetNearByCabs.svc/rest/nearby").buildUpon();
        builder.appendQueryParameter("Lat", String.valueOf(x))
                .appendQueryParameter("Lng", String.valueOf(y))
                .appendQueryParameter("SuggestedRadiusMeters", "5000")
                .appendQueryParameter("Mobile", "***");

        String url = builder.build().toString();
        String outputData =getRequest.GET(url);
        System.out.println(outputData);
        System.out.println(url);
        JSONObject json;

        try {

            outputData = outputData.substring(outputData.indexOf(">") + 1);
            outputData = outputData.replace("</string>", "");
            System.out.println(outputData);

            json = new JSONObject(outputData);
            JSONArray data = json.getJSONArray("Cablist");
            for (int i = 0; i < data.length(); i++) {
                JSONObject jsonObject = data.getJSONObject(i);
                CabData cabData = new CabData();
                cabData.cabCompany = CabData.CAB_MERU;
                cabData.estimate = jsonObject.getString("TrafficETA");
                cabData.localizedDisplayName = jsonObject.getString("Brand");
                cabDatas.add(cabData);
            }

        }catch (JSONException e){
            e.printStackTrace();
        }
        return cabDatas;
    }


    //Google Places API

    public List<LocationData> locationDataList() throws Exception{
        String val = null;
        List<LocationData> locationDatas = new ArrayList<LocationData>();
        Uri.Builder builder = Uri.parse("https://maps.googleapis.com/maps/api/place/nearbysearch/json").buildUpon();
        builder.appendQueryParameter("location", String.valueOf(x)+","+String.valueOf(y))
                .appendQueryParameter("radius", "1000")
                .appendQueryParameter("key","***");

        String url = builder.build().toString();
        String outputData =getRequest.GET(url);
        System.out.println(outputData);
        System.out.println(url);
        JSONObject json;

        try {
            json = new JSONObject(outputData);
            JSONArray jsonArray = json.getJSONArray("results");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                LocationData locationData = new LocationData();
                locationData.setName(jsonObject.getString("name"));
                JSONArray types = jsonObject.getJSONArray("types");

                for (int j=0; j< types.length(); j++){
                    val = types.getString(j);
                }
                locationData.setType(val);
                locationDatas.add(locationData);
            }
        }
        catch (JSONException e){
            e.printStackTrace();
        }
        return locationDatas;
    }

}
