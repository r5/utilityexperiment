package com.battery.ubertestapp;

/**
 * Created by namrata on 1/28/15.
 */
public class CabData {
    public String localizedDisplayName;
    public String displayName;

    public String cabCompany;

    public static final String CAB_OLA = "Ola";
    public static final String CAB_UBER = "Uber";
    public static final String CAB_TFS = "Taxi For Sure";
    public static final String CAB_MERU = "Meru";


    public String getEstimate() {
        return estimate;
    }

    public void setEstimate(String estimate) {
        this.estimate = estimate;
    }

    public String estimate;
    public String productId;

    public String getLocalizedDisplayNameDisplayName() {
        return localizedDisplayName;
    }

    public void setLocalizedDisplayName(String localizedDisplayName) {
        this.localizedDisplayName = localizedDisplayName;
    }

    @Override
    public String toString() {
        return displayName + "," + estimate + "," + productId + "," + localizedDisplayName;
    }
}
