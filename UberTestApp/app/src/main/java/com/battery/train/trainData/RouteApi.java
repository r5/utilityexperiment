package com.battery.train.trainData;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;

import com.battery.main.GetRequest;
import com.battery.train.PnrActivity;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by namrata on 2/23/15.
 */
public class RouteApi {

    public String trainno = PnrActivity.trainno;
    private final Context mContext;
    GetRequest getRequest = new GetRequest();

    public RouteApi(Context context) {
        this.mContext = context;
        String addr = "";
        getLocationFromAddress(addr);
    }

    public LatLng getLocationFromAddress(String strAddress) {

        Geocoder coder = new Geocoder(mContext);
        List<Address> address;
        LatLng p1 = null;
        try {
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }
            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();
            p1 = new LatLng(location.getLatitude(), location.getLongitude() );

        } catch (Exception ex) {

            ex.printStackTrace();
        }
        return p1;
    }

    public List<RouteData> routeDataList() throws Exception{
        List<RouteData> routeDatas = new ArrayList<RouteData>();
        Uri.Builder builder = Uri.parse("http://api.erail.in/fullroute").buildUpon();
        builder.appendQueryParameter("key", "***")
                .appendQueryParameter("trainno" , trainno);


        String url = builder.build().toString();
        String outputData =getRequest.GET(url);
        System.out.println(outputData);
        System.out.println(url);
        JSONObject jsonObject;

        try {
            jsonObject = new JSONObject(outputData);
            JSONObject result = jsonObject.getJSONObject("result");
            JSONArray route = result.getJSONArray("route");
            for (int i = 0; i < route.length(); i++) {
                JSONObject station = route.getJSONObject(i);
                RouteData routeData = new RouteData();
                routeData.setName(station.getString("name"));
                routeData.setArrival(station.getString("arr"));
                routeData.setDeparture(station.getString("dep"));
                String isStop = station.getString("stop");
                if (isStop.equals("Y")){
                    routeData.setStop(true);
                }else {
                    routeData.setStop(false);
                }
                routeData.setStationLatLng(getLocationFromAddress(routeData.name));
                routeDatas.add(routeData);
            }
            }catch (Exception e){
            e.printStackTrace();
        }
        return routeDatas;
    }
}
