package com.battery.train.pnr;

import android.net.Uri;

import com.battery.main.GetRequest;
import com.battery.train.PnrActivity;
import com.battery.train.PnrData;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by namrata on 2/21/15.
 */
public class ErailApi {

    String pnr = PnrActivity.pnr;

    GetRequest getRequest = new GetRequest();

    public List<PnrData> erailPnrDataList() throws Exception {
        List<PnrData> pnrDatas = new ArrayList<PnrData>();
        Uri.Builder builder = Uri.parse("http://api.erail.in/pnr").buildUpon();
        builder.appendQueryParameter("key", "***")
                .appendQueryParameter("pnr" , pnr);

        String url = builder.build().toString();
        String outputData =getRequest.GET(url);
        System.out.println(outputData);
        System.out.println(url);
        JSONObject jsonObject;

        try {
            jsonObject = new JSONObject(outputData);
            PnrData pnrData = new PnrData();
            JSONObject result = jsonObject.getJSONObject("result");
            pnrData.setPnrNum(result.getString("pnr"));
            pnrData.setTrainClass(result.getString("cls"));
            pnrData.setTrainNum(result.getString("trainno"));
            pnrData.setTrainName(result.getString("name"));
            pnrData.setToStation(result.getString("to"));
            pnrData.setFromStation(result.getString("from"));
            pnrData.setBoardingPoint(result.getString("brdg"));
            pnrData.setDate(result.getString("journey"));
            pnrDatas.add(pnrData);
        }catch (Exception e){
            e.printStackTrace();
        }
        return pnrDatas;
    }

}
