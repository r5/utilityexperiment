package com.battery.train;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.battery.train.trainData.RouteApi;
import com.battery.train.trainData.RouteData;
import com.battery.ubertestapp.R;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

/**
 * Created by namrata on 2/23/15.
 */
public class RouteActivity extends FragmentActivity {

    private GoogleMap mMap;
    RouteApi routeApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route);
        setUpMapIfNeeded();
        new ShowRoute().execute();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    private void setUpMapIfNeeded() {
        if (mMap == null) {
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.route_map)).getMap();
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    private void setUpMap() {
        mMap.addMarker(new MarkerOptions().position(new LatLng(28, 77)).title("You are here").icon(BitmapDescriptorFactory.fromResource(R.drawable.blue))).showInfoWindow();
        CameraUpdate center= CameraUpdateFactory.newLatLng(new LatLng(28, 77));
        CameraUpdate zoom=CameraUpdateFactory.zoomTo(14);
        mMap.moveCamera(center);
        mMap.animateCamera(zoom);
    }

    class ShowRoute extends AsyncTask<Void, Void, List<RouteData>> {

        // TODO resolve the exception while plotting the points on the map

        @Override
        protected List<RouteData> doInBackground(Void... params) {
            List<RouteData> data = null;
            try {
                routeApi = new RouteApi(getApplicationContext());
                data = routeApi.routeDataList();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return data;
        }

        @Override
        protected void onPostExecute(List<RouteData> routeDatas) {
            super.onPostExecute(routeDatas);
            for (final RouteData routeData : routeDatas) {
                if (routeData.stop){
                    mMap.addMarker(new MarkerOptions().position(routeData.getStationLatLng()).title(routeData.getName() + " at " + routeData.getArrival())).showInfoWindow();
                }
            }
        }
    }
}
