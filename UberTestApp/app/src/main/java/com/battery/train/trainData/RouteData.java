package com.battery.train.trainData;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by namrata on 2/23/15.
 */
public class RouteData {

    public String name;
    public String arrival;
    public String departure;
    public boolean stop;
    public LatLng stationLatLng;


    public LatLng getStationLatLng() {
        return stationLatLng;
    }

    public void setStationLatLng(LatLng stationLatLng) {
        this.stationLatLng = stationLatLng;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArrival() {
        return arrival;
    }

    public void setArrival(String arrival) {
        this.arrival = arrival;
    }

    public String getDeparture() {
        return departure;
    }

    public void setDeparture(String departure) {
        this.departure = departure;
    }

    public boolean isStop() {
        return stop;
    }

    public void setStop(boolean stop) {
        this.stop = stop;
    }
}
