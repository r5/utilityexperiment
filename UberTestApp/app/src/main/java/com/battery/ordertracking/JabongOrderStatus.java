package com.battery.ordertracking;

import android.app.Activity;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.TextView;

import com.battery.ubertestapp.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;

/**
 * Created by namrata on 2/18/15.
 */
public class JabongOrderStatus extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracking);
        Track track = new Track();
        track.execute();
    }

    private class Track extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {
            String postParams = null;
            try {
                String tinyurl = "http://jbo.ng/CHyPwbQ";
                String url = shortUrlForJabong(tinyurl);
                String page = GET(url);
                postParams = trackingInfo(page);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println(e);
            }
            return postParams;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            TextView text = (TextView) findViewById(R.id.tracking);
            text.setText(result);
        }
    }

    private final String USER_AGENT = "Mozilla/5.0";

    public String GET(String url) throws Exception {

        HttpClient httpclient = new DefaultHttpClient();
        HttpGet request = new HttpGet(url);
        request.addHeader("User-Agent", USER_AGENT);

        HttpResponse httpResponse = httpclient.execute(request);

        System.out.println("Response Code : " + httpResponse.getStatusLine().getStatusCode());

        BufferedReader rd = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));

        StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        return result.toString();
    }

    public String trackingInfo(String html) throws UnsupportedEncodingException {

        Document doc = Jsoup.parse(html);
        Element currentLocation = doc.select("h3.fs12.clr-green.mb10.pl5.pr5.f-normal").first();

        System.out.println(currentLocation.toString());

        return currentLocation.toString();
    }

    public String shortUrlForJabong(String tinyurl) throws Exception{

        String longurl = "";
        Uri.Builder builder = Uri.parse("http://api.unshorten.it").buildUpon();
        builder.appendQueryParameter("shortURL", tinyurl)
        .appendQueryParameter("responseFormat", "json")
        .appendQueryParameter("apiKey", "o8PC301bzI7Fe6B6vge7qlWSyrsC2ayo");

        String url = builder.build().toString();
        String outputData =GET(url);
        System.out.println(outputData);
        System.out.println(url);
        JSONObject json;

        try {
            json = new JSONObject(outputData);
            longurl = json.getString("fullurl");
        } catch (Exception e){
            e.printStackTrace();
        }
        return longurl;
    }
}
