package com.battery.dnd;

import android.content.Context;
import android.widget.ArrayAdapter;

import java.util.List;

/**
 * Created by namrata on 3/3/15.
 */
public class DndAdapter extends ArrayAdapter{

    private Context mContext;
    private List<DndData> dndDatas;

    public DndAdapter(Context context, int resource, List<DndData> dndDatas) {
        super(context, resource);
        mContext = context;
        this.dndDatas = dndDatas;
    }

    @Override
    public int getCount() {
        return dndDatas.size();
    }

    @Override
    public Object getItem(int position) {
        return dndDatas.get(position);
    }


}
