package com.battery.dnd;

/**
 * Created by namrata on 3/2/15.
 */
public class DndData {

    public String mobileNumber;
    public String dndStatus;
    public String label;
    public String value;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }


    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDndStatus() {
        return dndStatus;
    }

    public void setDndStatus(String dndStatus) {
        this.dndStatus = dndStatus;
    }

    public String getMobilenumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

}
