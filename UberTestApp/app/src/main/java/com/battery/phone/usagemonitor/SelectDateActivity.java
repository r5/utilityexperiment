package com.battery.phone.usagemonitor;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;

import com.battery.ubertestapp.R;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by namrata on 2/20/15.
 */
public class SelectDateActivity extends Activity {

    public int startYear, startMonth, startDay, endYear, endMonth, endDay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        showDatePicker();
    }

    public void showDatePicker() {
        LayoutInflater inflater = (LayoutInflater) getLayoutInflater();
        View customView = inflater.inflate(R.layout.activity_duration, null);

        final DatePicker dpStartDate = (DatePicker) customView.findViewById(R.id.dpStartDate);
        final DatePicker dpEndDate = (DatePicker) customView.findViewById(R.id.dpEndDate);

        // Build the dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(customView); // Set the view of the dialog to your custom layout
        builder.setTitle("Select start and end date");
        builder.setPositiveButton("Show Usage Details", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which) {
                long startDateLong = dpStartDate.getCalendarView().getDate();
                long endDateLong = dpEndDate.getCalendarView().getDate();
                dialog.dismiss();
                Intent intent = new Intent(getApplicationContext(), UsageMonitorActivity.class);
                intent.putExtra("startDate" , startDateLong);
                intent.putExtra("endDate" , endDateLong);
                startActivity(intent);
            }});
        builder.create().show();
    }
}
