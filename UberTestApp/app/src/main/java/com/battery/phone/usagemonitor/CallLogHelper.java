package com.battery.phone.usagemonitor;

/**
 * Created by namrata on 2/19/15.
 */
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CallLog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CallLogHelper {

    public static List<CallLogData> getAllCallLogs(ContentResolver cr, long startDate, long endDate) {
        // reading all data in descending order according to DATE
        String strOrder = android.provider.CallLog.Calls.DATE + " DESC";
        Uri callUri = Uri.parse("content://call_log/calls");
        Cursor curCallLogs = cr.query(callUri, null,
                android.provider.CallLog.Calls.DATE + ">" + startDate
                        +" and " + android.provider.CallLog.Calls.DATE + " < " + endDate, null, strOrder);
        List<CallLogData> callLogDataList = new ArrayList<CallLogData>();

        while (curCallLogs.moveToNext()){
            CallLogData callLogData = new CallLogData();
            callLogData.setLogNumber(curCallLogs.getString(curCallLogs.getColumnIndex(CallLog.Calls.NUMBER)));
            Long wDate = curCallLogs.getLong(curCallLogs.getColumnIndex(CallLog.Calls.DATE));
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            String dateString = formatter.format(new Date(wDate));
            callLogData.setLogDate(dateString);
            callLogData.setLogTime(curCallLogs.getLong(curCallLogs.getColumnIndex(CallLog.Calls.DURATION)));
            callLogData.setLogType(curCallLogs.getInt(curCallLogs.getColumnIndex(CallLog.Calls.TYPE)));
            callLogDataList.add(callLogData);
        }
        return callLogDataList;
    }
}
